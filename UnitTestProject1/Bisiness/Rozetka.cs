﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObject.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTestProject1.Decorator;

namespace UnitTestProject1.Bisiness
{
    class Rozetka
    {
        public IWebDriver SetUpDriver(string url)
        {
            IWebDriver driver= new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
            return driver;
        }
        public void FindListOfProductsOnHomePage(DecoratorWriteToFile decorator,IWebDriver driver,string product) 
        {
            decorator.Page("HomePage", true);
            HomePage home = new HomePage(driver);
            decorator.Method("FindsByText", true);
            home.FindsByText(product);
            decorator.Method("FindsByText", false);
            decorator.Page("HomePage", false);
        }
        public double FindTheCostlyItemFromListOfProductsOnSearchPage_ReturtShopingCartCoust(DecoratorWriteToFile decorator,IWebDriver driver, string name) 
        {
            decorator.Page("SearchPage", true);
            SearchPage search = new SearchPage(driver);

            decorator.Method("FindsByTextBrand", true);
            search.FindsByTextBrand(name);
            decorator.Method("FindsByTextBrand", false);

            decorator.Method("Rang", true);
            search.Rang();
            decorator.Method("Rang", false);

            decorator.Method("AddToShoppingCartFirstElement", true);
            search.AddToShoppingCartFirstElement();
            decorator.Method("AddToShoppingCartFirstElement", false);

            decorator.Method("OpenShoppingCart", true);
            search.OpenShoppingCart();
            decorator.Method("OpenShoppingCart", false);

            decorator.Page("SearchPage", false);
            return search.ShoppingCartPrice();
        }
        public double FindItemPriceOnShopingCartPage_ReturtShopingCartCoust(DecoratorWriteToFile decorator,IWebDriver driver)
        {
            decorator.Page("ShoppingCartPage", true);
            ShoppingCartPage shoppingCart = new ShoppingCartPage(driver);
            decorator.Page("ShoppingCartPage", false);
            return shoppingCart.ShoppingCartPrice();
        }
    }
}
