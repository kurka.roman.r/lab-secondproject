﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnitTestProject1.Bisiness;
using UnitTestProject1.Decorator;

namespace PageObject
{
     [TestFixture]
     [Parallelizable(ParallelScope.All)]
     public class Tests: TestBase
     {
         [TestMethod]
         [TestCaseSource(typeof(TestBase), "TestData2")]
        public void Test1(string product, string name, double priceless,string url)
        {
            decorator.Test("Test1 start work");
            decorator.Test("data");
            decorator.Test(product + " " + name + " " + priceless.ToString() + " " + url);
            Rozetka rozetka = new Rozetka();
            IWebDriver driver = rozetka.SetUpDriver(url);
            rozetka.FindListOfProductsOnHomePage(decorator,driver, product);
            NUnit.Framework.Assert.IsTrue(rozetka.FindTheCostlyItemFromListOfProductsOnSearchPage_ReturtShopingCartCoust(decorator,driver, name) > priceless 
                || rozetka.FindItemPriceOnShopingCartPage_ReturtShopingCartCoust(decorator,driver) > priceless);

            driver.Quit();
            decorator.Test("Test1 end work");
        }

     }

     public class TestBase
     {
        public DecoratorWriteToFile decorator;
        [SetUp]
         public void Setup()
         {
            decorator = new DecoratorWriteToFile();
            decorator.Test("Setup");
         }

         [TearDown]
         public void CleanUp()
         {
            decorator.Test("CleanUp");
        }
         private static IEnumerable<TestCaseData> TestData2()
         {
             var doc = XDocument.Load(System.AppDomain.CurrentDomain.BaseDirectory+"../../data/XMLFile1.xml");
             var res = from vars in doc.Descendants("data")
                       let product = vars.Attribute("product").Value
                       let name = vars.Attribute("name").Value
                       let priceless = Convert.ToDouble(vars.Attribute("priceless").Value)
                       let url = vars.Attribute("url").Value
                       select new TestCaseData(product, name, priceless, url);
             foreach (TestCaseData t in res)
                 yield return t;
         }
     }
}