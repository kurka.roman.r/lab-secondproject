﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1.Decorator
{


    public class DecoratorWriteToFile
    {
        public static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ConcreteLogToFile simple;
        Page_Text page_Text; Method_Text method_Text;
        Start_Text start_Text_page, start_Text_method;
        Complete_Text complete_Text_page, complete_Text_method;
        public DecoratorWriteToFile() 
        {
            simple = new ConcreteLogToFile();

            page_Text = new Page_Text(simple);
            start_Text_page = new Start_Text(page_Text);
            complete_Text_page = new Complete_Text(page_Text);

            method_Text = new Method_Text(simple);
            start_Text_method = new Start_Text(method_Text);
            complete_Text_method = new Complete_Text(method_Text);
        }

        public void Test(string text)
        {
            Log.Info(simple.WriteToFile() + text);
        }
        public void Page(string text, bool state)
        {
            if(state)
                Log.Info(start_Text_page.WriteToFile() + "(" + text + ")");
            else
                Log.Info(complete_Text_page.WriteToFile() + "(" + text + ")");
        }
        public void Method(string text, bool state)
        {
            if (state)
                Log.Info(start_Text_method.WriteToFile() + "(" + text + ")");
            else
                Log.Info(complete_Text_method.WriteToFile() + "(" + text + ")");
        }
    }

    public abstract class LogToFile
    {
        public abstract string WriteToFile();
    }
    class ConcreteLogToFile : LogToFile
    {
        public override string WriteToFile()
        {
            return "Test ";
        }
    }
    abstract class Decorator : LogToFile
    {
        protected LogToFile logToFile;

        public Decorator(LogToFile _logToFile)
        {
            this.logToFile = _logToFile;
        }

        public void SetLogToFile(LogToFile _logToFile)
        {
            this.logToFile = _logToFile;
        }
        public override string WriteToFile()
        {
            if (this.logToFile != null)
            {
                return this.logToFile.WriteToFile();
            }
            else
            {
                return string.Empty;
            }
        }
    }
    class Page_Text : Decorator
    {
        public Page_Text(LogToFile comp) : base(comp)
        {
        }
        public override string WriteToFile()
        {
            return base.WriteToFile() + "page ";
            
        }
    }
    class Method_Text : Decorator
    {
        public Method_Text(LogToFile comp) : base(comp)
        {
        }
        public override string WriteToFile()
        {
            return base.WriteToFile() + "method ";

        }
    }
    class Start_Text : Decorator
    {
        public Start_Text(LogToFile comp) : base(comp)
        {
        }

        public override string WriteToFile()
        {
            return base.WriteToFile() + "start ";
        }
    }
    class Complete_Text : Decorator
    {
        public Complete_Text(LogToFile comp) : base(comp)
        {
        }

        public override string WriteToFile()
        {
            return base.WriteToFile() + "complete ";
        }
    }
}
