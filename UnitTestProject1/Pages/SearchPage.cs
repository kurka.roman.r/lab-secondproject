﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using UnitTestProject1.Pages;

namespace PageObject.Pages
{
    class SearchPage : BasePage
    {
        public SearchPage(IWebDriver driver) : base(driver){}

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),'Бренд')]/ancestor::button/following-sibling::div//li/a")]
        private IList<IWebElement> SearchListBrand;
        [FindsBy(How = How.XPath, Using = "//select/option[text()='Від дорогих до дешевих']")]
        private IWebElement Costlier;
        [FindsBy(How = How.XPath, Using = "//div[@class='goods-tile__prices']//app-buy-button/button")]
        private IList<IWebElement> ShoppingCartButtons;
        [FindsBy(How = How.XPath, Using = "//header//ul/li/rz-cart/button")]
        private IWebElement ShoppingCart;
        [FindsBy(How = How.XPath, Using = "//div[@class='cart-receipt__sum-price']/span[@class]/preceding-sibling::span")]
        private IWebElement ShoppingCartWindow;

        
        public void FindsByTextBrand(string text)
        {
            Thread.Sleep(4000);
            SearchListBrand.Where(e => e.Text.Contains(text)).First().Click();
        }

        public void Rang() 
        {
            Costlier.Click();
        }
        public void AddToShoppingCartFirstElement()
        {
            Thread.Sleep(2000);
            ShoppingCartButtons.First().Click();
        }
        public void OpenShoppingCart()
        {
            ShoppingCart.Click();
        }
        public double ShoppingCartPrice()
        {
            return Convert.ToDouble(ShoppingCartWindow.Text);
        }
    }
}
