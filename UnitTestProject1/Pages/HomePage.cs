﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Text;
using UnitTestProject1.Pages;

namespace PageObject.Pages
{
    class HomePage: BasePage
    {
        public HomePage(IWebDriver driver):base(driver) {}

        [FindsBy(How = How.XPath, Using = "//input[@name='search']")]
        private IWebElement SearchInput;
        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Знайти')]")]
        private IWebElement SearchButton;

        public void FindsByText(string text) 
        {
            SearchInput.SendKeys(text);
            SearchButton.Click();
        }
    }

}
